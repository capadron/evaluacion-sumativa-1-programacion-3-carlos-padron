import re

if __name__ == '__main__':
    print('Taller 1 Programacion 3')
    NombreLadron = input("Cual es su Nombre: ")
    i = 10  #Variable para while loop.
    c = 0   #Contador para ver la cantidad de intentos.
    while i > 0 and i <= 10:
        t = input("Palabra Clave : ")   #variable para la clave, pedir el imput de la misma.
        if re.match(t, "Abrete Sesamo") or re.match(t, "Ábrete Sésamo"):    #Contraseña correcta.
            print("Se Abrio La Puerta")
            i = 20
            c = c + 1
        else:
            print("Nada Paso")
            i = i - 1
            c = c + 1
    if i == 20:
        print("Felicidades ",NombreLadron,", Usaste la contraseña correcta en ",c," Intento(s).")
    else:
        print(NombreLadron,"La Puerta se Cerrara Para Siempre...")


